import java.util.Objects;

public class HolidayOOPs {
    public static class Holiday
    {
        private String name;
        private int day;
        private String month;

        Holiday(String name, int day, String month)
        {
            this.name = name;
            this.day = day;
            this.month = month;
        }

    }

    public static boolean inSameMonth(String str1, String str2){
        return Objects.equals(str1, str2);
    }

    public static double avgDate(Holiday[] arr){
        double sum = 0;
        for (Holiday holiday : arr) {
            sum += holiday.day;
        }

        return sum/(double)(arr.length);
    }


    public static void main (String[] args)
    {

        Holiday holiday1 = new Holiday("abc", 23, "June");
        Holiday holiday2 = new Holiday("xyz", 25, "June");
        Holiday holiday3 = new Holiday("Independence Day", 4, "July");

        Holiday[] arr = new Holiday[3];
        arr[0] = holiday1;
        arr[1] = holiday2;
        arr[2] = holiday3;

        System.out.println(inSameMonth(holiday1.month, holiday2.month));
        System.out.println(String.format("%.2f",avgDate(arr)));

    }
}

import java.util.Scanner;

public class MagicSquare {
    public static boolean isMagicSquare(int[][] matrix, int n)
    {
        int row_sum = 0, col_sum = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
            {
                row_sum += matrix[i][j];
                col_sum += matrix[j][i];
            }
            if (row_sum != col_sum)
                return false;
        }

        int sumd1 = 0,sumd2=0;
        for (int i = 0; i < n; i++)
        {
            sumd1 += matrix[i][i];
            sumd2 += matrix[i][n-1-i];
        }
        return sumd1 == sumd2 && row_sum == sumd1;
    }

    public static void main(String[] args)
    {

        Scanner scan = new Scanner(System.in);
        int n;
        n = scan.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                matrix[i][j] = scan.nextInt();


        if (isMagicSquare(matrix,n))
            System.out.println("Yes");
        else
            System.out.println("No");
    }

}

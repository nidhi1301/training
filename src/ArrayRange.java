import java.util.Scanner;

public class ArrayRange {

    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        int n;
        n = scan.nextInt();
        int min=Integer.MAX_VALUE, max=Integer.MIN_VALUE;
        int[] arr = new int[n];
        for(int i=0; i < n; i++) {
            arr[i] = scan.nextInt();
            if(arr[i]<min) min=arr[i];
            if(arr[i]>max) max=arr[i];
        }
        System.out.println(max-min);
    }
}

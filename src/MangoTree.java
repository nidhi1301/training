import java.util.Scanner;

public class MangoTree {
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int rowCount, colCount, treeNum;
        rowCount = scan.nextInt();
        colCount = scan.nextInt();
        treeNum = scan.nextInt();

        if(rowCount*colCount>=treeNum) {
            if(treeNum<=colCount || treeNum%colCount==1 || treeNum%colCount==0)
                System.out.println("yes");
        }
        else
            System.out.println("no");
    }
}

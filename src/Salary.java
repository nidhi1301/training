import java.util.Scanner;

public class Salary {

    public static int calculateSalary(int[] daily_hrs) {
        int total_hrs = 0;
        int salary = 0;
        for(int i=0; i < 7; i++) {
            if(i!=0 && i!=6)
                total_hrs += daily_hrs[i];
        }

        for(int i=0; i<7; i++){
            if(i!=0 && i!=6){
                salary += (daily_hrs[i]*100);
                int extra_hrs = daily_hrs[i]-8;
                if(extra_hrs>0)
                    salary += (extra_hrs*15);
            }
            else{
                if(i==0)
                    salary += (daily_hrs[i]*150);
                else
                    salary += (daily_hrs[i]*125);
            }
        }


        if(total_hrs > 40){
            int weekly_extra_hrs = total_hrs - 40;
            salary += (weekly_extra_hrs*125);
        }

        return salary;
    }
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int[] daily_hrs = new int[7];
        for(int i=0; i < 7; i++) {
            daily_hrs[i] = scan.nextInt();
        }
        System.out.println(calculateSalary(daily_hrs));
    }

}

import java.util.Arrays;
import java.util.Scanner;

public class MeanMedianMode {
    public static double findMean(int[] arr, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += arr[i];

        return (double) sum / (double) n;
    }

    public static double findMedian(int[] arr, int n) {
        Arrays.sort(arr);
        if (n % 2 != 0)
            return arr[n / 2];

        return (double) (arr[(n - 1) / 2] + arr[n / 2]) / 2.0;
    }

    public static int findMode(int[] arr, int n) {
        int max = 0, maxCount = 0;
        for (int i = 0; i < n; ++i) {
            int count = 0;
            for (int j = 0; j < n; ++j) {
                if (arr[j] == arr[i]) ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                max = arr[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n;
        n = scan.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }

        System.out.println("Mean = " + String.format("%.2f", findMean(arr, n)));
        System.out.println("Median = " + String.format("%.2f", findMedian(arr, n)));
        System.out.println("Mode = " + findMode(arr, n));

    }

}
